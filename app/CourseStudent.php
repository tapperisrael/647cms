<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class CourseStudent extends Model
{

    use Mediable;

    public function courses()
    {
        return $this->belongsTo(Courses::class);
    }

}
