<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\View\View;
Use Redirect;



class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(CompanyCategory $companyCategory)
    {
        //echo json_encode($companyCategory);
        $Companies = $companyCategory->companies;
       // echo json_encode($Categories);
        $isEdit = true;
        $Delete = true;
         $Url = "/company";
         $fileds = array('','שם חברה','כתובת','טלפון','פרטים');
         $rows = array('id','title','address','phone2','description');
        //return view('company.index');
       return view('company.index', ['name' => 'id','Title' => 'עסקים' ,'Description' => 'רשימת כל בתי העסק הקיימים'  ,'Companies' => $Companies , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //echo $id;
       // $Companies = Company::find(1); // Company::find($id);
        //$Companies  = \App\company::where('region_id','=',$id)->orderBy('id','asc')->get();
        //echo $Companies;
        //return view('company.index',['Companies' => $Companies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyCategory $companyCategory ,Company $company)
    {
        $company->delete();
        return redirect("company_categories/{$companyCategory->id}/companies");
      //  return view('company.index',['Companies' => $Companies]);
    }
}
