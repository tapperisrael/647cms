<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumber;
use Plank\Mediable\Mediable;


class User extends Authenticatable
{
    use Notifiable , Mediable;

    protected $fillable = ['name', 'email', 'phone', 'city', 'points'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['phone3'];

    public function getPhone3Attribute()
    {
        try{
            $p = phone($this->phone, ['IL'], PhoneNumberFormat::NATIONAL);
            return $p;
        }
        catch(\Exception $e){
            return $this->phone;
        }
    }




}

