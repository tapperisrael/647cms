@extends('layouts.app')

@section('content')
    <div class="formWrapper">
        <div class="row DirectionRtl">
            <div class="col-lg-offset-2 col-lg-10">
                <h1 class="page-header">הוספת מאמן חדש</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row DirectionRtl" >
            <div class="col-lg-12">
                <div class="panel panel-default" >
                    <div class="panel-heading " >
                        עדכן מאמן חדש
                    </div>
                    <div class="panel-body">
                        <div class="row ">
                            <div class="col-lg-offset-2 col-lg-10">
                                    <form method="post" action="{{url('Courses')}}">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>כתובת</label>
                                                    <input class="form-control" value="" name="address">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>שם הקורס</label>
                                                    <input class="form-control" value="" name="title">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>בחר תזונאית</label>
                                                    <select class="form-control mb-10" name="coach_id">
                                                        @foreach($Coaches as $item)
                                                            @if($item->is_nutritionist == 1 )
                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>בחר מאמן</label>
                                                    <select class="form-control mb-10" name="nutritionist_id">
                                                        @foreach($Coaches as $item)
                                                            @if($item->is_nutritionist == 0 )
                                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>


                                        <button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important;" >הוסף מאמן</button>
                                    {{csrf_field()}}
                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

@endsection





